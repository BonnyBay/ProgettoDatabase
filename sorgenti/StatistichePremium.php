<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="Web2.css">
   <title>Statistiche Utenti Premium</title>
</head>
<body>
  <div class="p-3 mb-2 bg-warning text-dark"><p align="left"><strong><h1>Statistiche degli utenti premium</h1></strong></p></div>
  <?php
    include 'connection.php';
    session_start();
     if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])and !isset($_SESSION['ricercatore'])){
       //echo (.$user);
       header("location:home.html");
     }
    if(isset($_SESSION['userSemplice'])){
       $username=$_SESSION['userSemplice'];
     }
    else if(isset($_SESSION['userPremium'])){
       $username=$_SESSION['userPremium'];
     }else{
      $username=$_SESSION['ricercatore'];
     }
     try{
       $sql="SELECT * FROM UtentiPremium";
       $res=$pdo->query($sql);
       echo'<table class= "table table-hover table-dark">';
       echo"<tr>";
       echo'<th scope="col">';
       echo"Utente";
       echo"</th>";
       echo'<th scope="col">';
       echo"Indice affidabilità";
       echo"</th>";
       echo"</tr>";           
       while($row=$res->fetch()) {

         echo'<tr>';
         echo"<td>".$row['nome']."</td>";
         echo"<td>".$row['indice']."</td>";
         echo"</tr>";
         // echo('<hr><br>Utente: '.$row['nomeUtente'].' Numero di segnalazioni: '.$row['nSegnalazioni']);
       }
       echo"</table>";
       echo '<p align="left"><a href="FormVisualizzaStatistiche.php">Torna all'."'".'area statistiche</a></p>';
    }
     catch(PDOException $e){
     echo $e->getMessage();
   }

    $pdo=null;
  ?>
</body>
</html>


