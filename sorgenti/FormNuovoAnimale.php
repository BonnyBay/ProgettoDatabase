<?php
   //sessioni da 24 min
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Aggiungi il tuo animale</title>
</head>
<body class="aggiungiAnimale">
    <?php
      echo "<h3> Benvenuto ".$_SESSION['ricercatore']."!";
      echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
    ?>
    <p align="center"><strong>Aggiungi un nuovo animale: </strong></p>
    <form id="FormVegetale" action="nuovoAnimale.php" method="post" align="center">
      Username: <?php echo $_SESSION['ricercatore']; ?><br><br>
      Livello vulnerabilità: <br>
      <div>
        <label>
          <input type="radio" class="radio" value="Alto" name="vulnerabilita" />Alto
        </label>
        <label>
          <input type="radio" class="radio" value="Medio" name="vulnerabilita" />Medio
        </label>
        <label>
          <input type="radio" class="radio" value="Basso" name="vulnerabilita" />Basso
        </label> 
      </div>
      Anno di classificazione:<br>
      <input type="number" name="annoClassificazione"><br><br>
      Nome latino dell'animale:<br>
      <input type="text" name="nomeLatino"><br><br>
      Nome italiano dell'animale:<br>
      <input type="text" name="nomeItaliano"><br><br>
      Classe:<br>
      <input type="text" name="classe"><br><br>
      Link alla pagina di wikipedia:<br>
      <input type="text" name="linkWiki"><br><br>
      Altezza: <br>
      <input type="number" min="0" name="altezza"><br><br>
      Peso: <br>
      <input type="number" min="0" name="peso"><br><br>
      Numero medio di prole: <br>
      <input type="number" min="0" name="nProle"><br><br>

      <input type="Submit" class="button button-block" value="Invia">
    </form>
</body>
</html>
