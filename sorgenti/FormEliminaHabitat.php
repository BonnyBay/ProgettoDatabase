<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Elimina Habitat</title>
</head>
<body>
    <?php
      echo "<h3> Benvenuto ".$_SESSION['ricercatore']."!";
      echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
    ?>
    <p align="center"><strong>Elimina un habitat presente nel database: </strong></p>
    <form id="FormHabitat" action="eliminaHabitat.php" method="post" align="center">
      Username: <?php echo $_SESSION['ricercatore']; ?><br><br>
      Nome dell'habitat da eliminare: <br>
      <input type="text" min="1" name="nomeHabitat"><br><br>
      <input type="Submit" class="button button-block" value="Invia">
    </form>
    <?php
    try{
      $sql='SELECT nome FROM HABITAT';
      $res=$pdo->query($sql);
      echo 'Habitat presenti nel database:<br>';
      while($row=$res->fetch()) {
        echo('<br>Nome: '.$row['nome']);
      }
    }
    catch(PDOException $e) {
       echo("Errore esecuzione query.");
       exit();
    }
    ?>
</body>
</html>
