<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="Web2.css">
   <title>Statistiche Escursioni</title>
</head>
<body>
  <div class="p-3 mb-2 bg-success text-white"><p align="left"><strong><h1>Statistiche delle escursioni</h1></strong></p></div> 
  <h4><div class="p-3 mb-2 bg-info text-white">Escursioni inserite</div></h4>
  <?php
    include 'connection.php';
    session_start();
     if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])and !isset($_SESSION['ricercatore'])){
       //echo (.$user);
       header("location:home.html");
     }

    if(isset($_SESSION['userSemplice'])){
       $username=$_SESSION['userSemplice'];
     }
    else if(isset($_SESSION['userPremium'])){
       $username=$_SESSION['userPremium'];
     }else{
      $username=$_SESSION['ricercatore'];
     }?>
     <p align="center"><strong>Iscriviti ad una escursione </strong></p>
   <form id="FormPartecipazioneEscursione" action="nuovaPartecipazioneEscursione.php" method="post" align="center">
     Username:
     <?php
       if(isset($_SESSION['userSemplice'])){
         echo $_SESSION['userSemplice'];
       }
       else{
         echo $_SESSION['userPremium'];
       }
     ?><br><br>
     ID Escursione:
     <input type="number" min="1" name="idescursione"><br><br>
     <input type="Submit" class="button button-block" value="Invia partecipazione">
   </form>
   <?php
     try{
      $sql="SELECT * FROM EscursioniPresenti";
      $res=$pdo->query($sql);
      echo'<table class= "table table-hover table-dark">';
      echo"<tr>";
      echo'<th scope="col">';
      echo"id";
      echo"</th>";
      echo'<th scope="col">';
      echo"Titolo";
      echo"</th>";
      echo'<th scope="col">';
      echo"Numero di partecipanti";
      echo"</th>";
      echo"</tr>";           
      while($row=$res->fetch()) {
         echo'<tr>';
         echo"<td>".$row['id']."</td>";
         echo"<td>".$row['titolo']."</td>";
         echo"<td>".$row['nPartecipanti']."</td>";
         echo"</tr>";
      }
       echo"</table>";
       echo '<p align="left"><a href="FormVisualizzaStatistiche.php">Torna all'."'".' area statistiche</a></p>';
    }
     catch(PDOException $e){
     echo $e->getMessage();
   }

    $pdo=null;
  ?>
</body>
</html>





