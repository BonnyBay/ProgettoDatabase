<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Il tuo profilo</title>
</head>
<body>
    <?php
      echo "<h3> Benvenuto ".$_SESSION['ricercatore']."!";
      echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
    ?>
    <p align="center"><strong>Aggiungi un nuovo Habitat: </strong></p>
    <form id="FormVegetale" action="modificaHabitat.php" method="post" align="center">
      Username: <?php echo $_SESSION['ricercatore']; ?><br><br>
      Nome dell'habitat: <br>
      <input type="text" min="1" name="nomeHabitat"><br><br>
      Descrizione<br>
      <textarea name="descrizione" rows="5" cols="40"></textarea><br><br>
      <input type="Submit" class="button button-block" value="Invia">
    </form>
    <?php
    try{
      $sql='SELECT nome,descrizione FROM HABITAT';
      $res=$pdo->query($sql);
      echo 'Specie presenti nel database:<br>';
      while($row=$res->fetch()) {
        echo('<br>Nome: '.$row['nome'].' - Descrizione: '.$row['descrizione']);
      }
    }
    catch(PDOException $e) {
       echo("Errore esecuzione query.");
       exit();
    }
    ?>
</body>
</html>
