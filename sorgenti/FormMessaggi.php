<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium']) and !isset($_SESSION['ricercatore'])){
    //echo (.$user);
    header("location:home.html");
  }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Messaggi</title>
</head>
<body>
    <?php
      if(isset($_SESSION['userSemplice'])){
        echo "<h3> Benvenuto ".$_SESSION['userSemplice']."!";
      }
      else if(isset($_SESSION['userPremium'])){
        echo "<h3> Benvenuto ".$_SESSION['userPremium']."!";
      }
      else{
        echo "<h3> Benvenuto ".$_SESSION['ricercatore']."!";
      }

      if(isset($_SESSION['userSemplice'])){
        echo '<p align="left"><a href="profiloSemplice.php">Torna alla tua area personale</a></p>';
      }
      else if(isset($_SESSION['userPremium'])){
        echo '<p align="left"><a href="profiloPremium.php">Torna alla tua area personale</a></p>';
      }
      else{
        echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
      }
      echo '<p align="left"><a href="messaggiRicevuti.php">Messaggi ricevuti</a></p>';
    ?>
    <p align="center"><strong>I tuoi Messaggi: </strong></p>
    <form id="InvioMessaggio" action="inviaMessaggio.php" method="post" align="center">
      Mittente:
      <?php
        if(isset($_SESSION['userSemplice'])){
          echo $_SESSION['userSemplice'];
        }
        else if(isset($_SESSION['userPremium'])){
          echo $_SESSION['userPremium'];
        }
        else{
          echo $_SESSION['ricercatore'];
        }
      ?><br><br>
      Destinatario: <br>
      <input type="text" name="destinatario"><br><br>
      Titolo del messaggio: <br>
      <input type="titolo" name="titolo"><br><br>
      Testo del messaggio<br>
      <textarea name="testo" rows="10" cols="40"></textarea><br><br>
      <input type="Submit" class="button button-block" value="Invia">
    </form>
    <?php
    try{
      $sql='SELECT nome FROM PROFILO';
      $res=$pdo->query($sql);
      echo 'Utenti presenti:<br>';
      while($row=$res->fetch()) {
        #non stampo il nome dell'utente che vuole inviare il messaggio, ma solo
        #i possibili destinatari
        if(isset($_SESSION['userSemplice'])){
          if($row['nome']!=$_SESSION['userSemplice']){
            echo('<br>Username: '.$row['nome']);
          }
        }
        else if(isset($_SESSION['userPremium'])){
          if($row['nome']!=$_SESSION['userPremium']){
            echo('<br>Username: '.$row['nome']);
          }
        }
        else{
          if($row['nome']!=$_SESSION['ricercatore']){
            echo('<br>Username: '.$row['nome']);
          }
        }
      }
    }
    catch(PDOException $e) {
       echo("Errore esecuzione query.");
       exit();
    }
    ?>
</body>
</html>
