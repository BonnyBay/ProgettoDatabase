<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
  $username=$_SESSION['ricercatore'];
  $vulnerabilita=$_POST["vulnerabilita"];
  $annoClassificazione=$_POST["annoClassificazione"];
  $nomeLatino=$_POST["nomeLatino"];
  $nomeItaliano=$_POST["nomeItaliano"];
  $classe=$_POST["classe"];
  $linkWiki=$_POST["linkWiki"];
  $altezza=$_POST["altezza"];
  $diametro=$_POST["diametro"];
  try{
    $sql="SELECT nomeLatino FROM VEGETALE WHERE nomeLatino=:lab1";
    $ifspecie=$pdo->prepare($sql);
    $ifspecie->bindValue(":lab1",$nomeLatino);
    $ifspecie->execute();
    $check=$ifspecie->rowCount();
    if($check==1){
      if(!empty($vulnerabilita) and !empty($annoClassificazione) and !empty($nomeLatino) and !empty($nomeItaliano) and !empty($classe) and !empty($linkWiki) and !empty($altezza) and !empty($diametro)){
        $result=$pdo->query("CALL ModificaVegetale( '$nomeLatino','$username','$vulnerabilita', '$annoClassificazione', '$nomeItaliano','$classe','$linkWiki','$altezza','$diametro');");
        echo '<script>alert("Vegetale modificato con successo!");
              window.location.href="FormModificaVegetale.php";
              </script>';
      }
      else{
        echo '<script>alert("Compilare tutti i campi per poter proseguire.");
              window.location.href="FormModificaVegetale.php";
              </script>';
      }
    }
    else{
      echo '<script>alert("Specie vegetale non presente nel database.");
            window.location.href="FormModificaVegetale.php";
            </script>';
    }
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  $pdo=null;

  // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'modifica vegetale', 'nomeLatino' => $nomeLatino, 'nomeItaliano' => $nomeItaliano, 'vulnerabilita' => $vulnerabilita, 'annoClassificazione' => $annoClassificazione, 'classe' => $classe, 'linkWiki'=> $linkWiki, 'altezza' => $altezza, 'diametro' => $diametro]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }
?>
