<?php
 include 'connection.php';
 //$user=$_POST["Username"];
 session_start();
 if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])){
   //echo (.$user);
   header("location:home.html");
 }
?>

<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="Web1.css">
   <title>Il tuo profilo</title>
</head>
<body>
 <?php
 if(isset($_SESSION['userSemplice'])){
   echo "<h3> Benvenuto ".$_SESSION['userSemplice']."!";
 }
 else{
   echo "<h3> Benvenuto ".$_SESSION['userPremium']."!";
 }
 if(isset($_SESSION['userSemplice'])){
   echo '<p align="left"><a href="profiloSemplice.php">Torna alla tua area personale</a></p>';
 }
 else{
   echo '<p align="left"><a href="profiloPremium.php">Torna alla tua area personale</a></p>';
 }
 ?>
   <p align="center"><strong>Iscriviti ad una escursione </strong></p>
   <form id="FormPartecipazioneEscursione" action="nuovaPartecipazioneEscursione.php" method="post" align="center">
     Username:
     <?php
       if(isset($_SESSION['userSemplice'])){
         echo $_SESSION['userSemplice'];
       }
       else{
         echo $_SESSION['userPremium'];
       }
     ?><br><br>
     ID Escursione:
     <input type="number" min="1" name="idescursione"><br><br>
     <input type="Submit" class="button button-block" value="Invia partecipazione">
   </form>

<br><br>

<div class="p-3 mb-2 bg-warning text-dark">
  <h2>Escursioni presenti:</h2>

   <?php
   try {
      $sql='SELECT * FROM ESCURSIONE';
      $res=$pdo->query($sql);
       echo'<table class= "table table-hover table-dark">';
       echo"<tr>";
       echo'<th scope="col">';
       echo"Id:";
       echo"</th>";
       echo'<th scope="col">';
       echo"NOME CREATORE:";
       echo"</th>";
       echo'<th scope="col">';
       echo"DATA:";
       echo"</th>";
       echo'<th scope="col">';
       echo"TITOLO:";
       echo"</th>";
       echo'<th scope="col">';
       echo"TRAGITTO:";
       echo"</th>";
       echo'<th scope="col">';
       echo"n MAX PARTECIPANTI:";
       echo"</th>";
       echo'<th scope="col">';
       echo"DESCRIZIONE:";
       echo"</th>";
       echo'<th scope="col">';
       echo"h PARTENZA:";
       echo"</th>";
       echo'<th scope="col">';
       echo"h RIENTRO:";
       echo"</th>";
       echo"</tr>";  

      while($row=$res->fetch()) {
        echo'<tr>';
        echo"<td>".$row['id']."</td>";
         echo"<td>".$row['nomeCreatore']."</td>";
         echo"<td>".$row['data']."</td>";
         echo"<td>".$row['titolo']."</td>";
         echo"<td>".$row['tragitto']."</td>";
         echo"<td>".$row['nPartecipanti']."</td>";
         echo"<td>".$row['descrizione']."</td>";
         echo"<td>".$row['orarioPartenza']."</td>";
         echo"<td>".$row['orarioRitorno']."</td>";
         echo"</tr>";
        
      }
      echo"</table>";

   }
   catch(PDOException $e) {
     echo("Errore esecuzione query.");
     exit();
   }
   ?>

</body>
</html>
