<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
  $username=$_SESSION['ricercatore'];
  $nomeSpecie=$_POST['nomeSpecie'];
  try{
    if(!empty($nomeSpecie)){
      $sql="SELECT nomeLatino FROM SPECIE WHERE nomeLatino=:lab1";
      $ifspecie=$pdo->prepare($sql);
      $ifspecie->bindValue(":lab1",$nomeSpecie);
      $ifspecie->execute();
      $check=$ifspecie->rowCount();
      if($check==1){
        $result=$pdo->query("CALL EliminaSpecie('$username','$nomeSpecie');");
        echo '<script>alert("Specie eliminata con successo!");
              window.location.href="FormEliminaSpecie.php";
              </script>';
      }
      else{
        echo '<script>alert("Specie non presente nel database!");
              window.location.href="FormEliminaSpecie.php";
              </script>';
      }
    }
    else{
      echo '<script>alert("Compilare tutti i campi per poter proseguire.");
            window.location.href="FormEliminaSpecie.php";
            </script>';
    }
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  $pdo=null;

   // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'elimina specie', 'nomeSpecie' => $nomeSpecie]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }

?>
