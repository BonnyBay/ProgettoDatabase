<?php
  include 'connection.php';
  session_start();
  $user=$_POST["Username"];
  $pw=$_POST["Password"];
  $password=md5($pw);
  try{
    // ----> ' OR '1'='1 ciao
    $sql=("SELECT nome,password FROM PROFILO WHERE ((nome=:lab1) and (password=:lab2));");
    $res=$pdo->prepare($sql);
    $res->bindValue(":lab1",$user);
    $res->bindValue(":lab2",$password);
    $res->execute();
    $count=$res->rowCount();
    if($count==1){
      try{
        $sql="SELECT tipoAccount FROM PROFILO WHERE nome=:lab1";
        $res=$pdo->prepare($sql);
        $res->bindValue(":lab1",$user);
        $res->execute();
        $tipoUtente=$res->fetch();
        if($tipoUtente['tipoAccount']=="UTENTE SEMPLICE"){
          $_SESSION['userSemplice']=$user;
          header("location:profiloSemplice.php");
        }
        else if($tipoUtente['tipoAccount']=="RICERCATORE"){
          $_SESSION['ricercatore']=$user;
          header("location:profiloRicercatore.php");
        }
        else if($tipoUtente['tipoAccount']=="UTENTE PREMIUM"){
          $_SESSION['userPremium']=$user;
          header("location:profiloPremium.php");
        }
      }
      catch(PDOException $e){
        echo $e->getMessage();
      }
    }
    else{
      echo '<script>alert("Username e/o password errati");
            window.location.href="home.html";
            </script>';
    }
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $user, 'azione' => 'login']);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }

?>
