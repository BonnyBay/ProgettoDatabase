<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="Web2.css">
   <title>Statistiche Campagne Fondi</title>
</head>
<body>
  <div class="p-3 mb-2 bg-success text-white"><p align="left"><strong><h1>Statistiche delle campagne fondi</h1></strong></p></div>
  <h4><div class="p-3 mb-2 bg-info text-white">Campagne fondi con stato APERTO</div></h4>
  <?php
    include 'connection.php';
    session_start();

     if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])and !isset($_SESSION['ricercatore'])){
       //echo (.$user);
       header("location:home.html");
     }

    if(isset($_SESSION['userSemplice'])){
       $username=$_SESSION['userSemplice'];
     }
    else if(isset($_SESSION['userPremium'])){
       $username=$_SESSION['userPremium'];
     }else{
      $username=$_SESSION['ricercatore'];
     }
     try{
       $sql="SELECT * FROM CampagneFondi";
       $res=$pdo->query($sql);
        echo'<table class= "table table-hover table-dark">';
        echo"<tr>";
        echo'<th scope="col">';
        echo"NOME CREATORE";
        echo"</th>";
        echo'<th scope="col">';
        echo"NOME RACCOLTA FONDI";
        echo"</th>";
        echo'<th scope="col">';
        echo"IMPORTO RACCOLTO";
        echo"</th>";
        echo'<th scope="col">';
        echo"IMPORTO MASSIMO";
        echo"</th>";
        echo'<th scope="col">';
        echo"DESCRIZIONE";
        echo"</th>";
        echo'<th scope="col">';
        echo"DATA INIZIO";
        echo"</th>";
        echo"</tr>";    
       while($row=$res->fetch()) {
        echo'<tr>';
        echo"<td>".$row['nomeCreatore']."</td>";
        echo"<td>".$row['nome']."</td>";
        echo"<td>".$row['importoRaccolto']."</td>";
        echo"<td>".$row['importoMax']."</td>";
        echo"<td>".$row['descrizione']."</td>";
        echo"<td>".$row['dataInizio']."</td>";
        echo"</tr>";
       }
       echo"</table>";
       echo '<p align="left"><a href="FormVisualizzaStatistiche.php">Torna all'."'".' area statistiche</a></p>';
    }catch(PDOException $e){
     echo $e->getMessage();
   }

    $pdo=null;
  ?>
</body>
</html>
