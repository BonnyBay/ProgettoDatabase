<?php
 include 'connection.php';
 //$user=$_POST["Username"];
 session_start();
 if( (!isset($_SESSION['userSemplice'])) and (!isset($_SESSION['userPremium'])) and (!isset($_SESSION['ricercatore'])) ){
   //echo (.$user);
   header("location:home.html");
 }
?>

<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <title>Il tuo profilo</title>
</head>
<body>
 <?php
   if(isset($_SESSION['userSemplice'])){
     echo "<h3> Benvenuto ".$_SESSION['userSemplice']."!";
   }
   else if(isset($_SESSION['userPremium'])){
     echo "<h3> Benvenuto ".$_SESSION['userPremium']."!";
   }
   else{
     echo "<h3> Benvenuto ".$_SESSION['ricercatore']."!";
   }

   if(isset($_SESSION['userSemplice'])){
     echo '<p align="left"><a href="profiloSemplice.php">Torna alla tua area personale</a></p>';
   }
   else if(isset($_SESSION['userPremium'])){
     echo '<p align="left"><a href="profiloPremium.php">Torna alla tua area personale</a></p>';
   }
   else{
     echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
   }
 ?>
   <p align="center"><strong>Aggiorna il tuo profilo:</strong></p>
   <form id="FormAggiornaProfilo" action="aggiornaProfilo.php" method="post" align="center" enctype="multipart/form-data">
     Username:
     <?php
       if(isset($_SESSION['userSemplice'])){
         echo $_SESSION['userSemplice'];
       }
       else if(isset($_SESSION['userPremium'])){
         echo $_SESSION['userPremium'];
       }
       else{
         echo $_SESSION['ricercatore'];
       }
     ?><br><br>
     Email:
     <input type="text" name="email"><br><br>
     Password:
     <input type="password" name="password" /><br><br>
     <div class="datiInput" name="foto">
       <br>FOTO: <input type="file" name="foto" class="form-control" aria-describedby="basic-addon1">
     </div><br>
     Professione:
     <input type="text" name="professione"><br><br>
     Anno di nascita:
     <input type="number" name="annoNascita"><br><br>
     <input type="Submit" class="button button-block" value="Aggiorna">
   </form>
</body>
</html>
