<?php
  $username= "root";
  $password= "root";
  try {
    $pdo = new PDO('mysql:host=localhost;dbname=ProgettoDB_Nature', $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec("set names utf8");
  }catch(PDOException $ex){
    echo("Connessione non riuscita");
    exit();
  }
?>
