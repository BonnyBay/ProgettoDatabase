<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="Web2.css">
   <title>Statistiche</title>
</head>
<body>
  <div class="p-3 mb-2 bg-success text-white"><p align="left"><strong><h1>Classifica delle specie con delle segnalazioni</h1></strong></p></div>
  <?php
    include 'connection.php';
    session_start();

     if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])and !isset($_SESSION['ricercatore'])){
       //echo (.$user);
       header("location:home.html");
     }

    if(isset($_SESSION['userSemplice'])){
       $username=$_SESSION['userSemplice'];
     }
    else if(isset($_SESSION['userPremium'])){
       $username=$_SESSION['userPremium'];
     }else{
      $username=$_SESSION['ricercatore'];
     }
     try{
       $sql="SELECT * FROM ClassificaSpecie";
       $res=$pdo->query($sql);
        echo'<table class= "table table-hover table-dark">';
        echo"<tr>";
        echo'<th scope="col">';
        echo"NOME SPECIE";
        echo"</th>";
        echo'<th scope="col">';
        echo"Numero Segnalazioni";
        echo"</th>";
        echo"</tr>";    
       while($row=$res->fetch()) {
          echo'<tr>';
          //mostriamo solo il numero di segnalazioni associate alle specie che sono state classificate e quindi alle specie che non sono sconosciute
          if(isset($row['nomeSpecie'])){
            echo"<td>".$row['nomeSpecie']."</td>";
            echo"<td>".$row['quantita']."</td>";
          }
          echo"</tr>";
       }
       echo"</table>";
       echo '<p align="left"><a href="FormVisualizzaStatistiche.php">Torna all'."'".' area statistiche</a></p>';
    }catch(PDOException $e){
     echo $e->getMessage();
   }

    $pdo=null;
  ?>
</body>
</html>
