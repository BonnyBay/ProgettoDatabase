<?php
 include 'connection.php';
 //$user=$_POST["Username"];
 session_start();
 if( (!isset($_SESSION['userSemplice'])) and (!isset($_SESSION['userPremium'])) and (!isset($_SESSION['ricercatore'])) ){
   //echo (.$user);
   header("location:home.html");
 }
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="Web1.css">
    <title>Nuova Donazione</title>
</head>
<body>
    <?php
       if(isset($_SESSION['userSemplice'])){
         echo "<h3> Benvenuto ".$_SESSION['userSemplice']."!";
       }
       else if(isset($_SESSION['userPremium'])){
         echo "<h3> Benvenuto ".$_SESSION['userPremium']."!";
       }
       else{
         echo "<h3> Benvenuto ".$_SESSION['ricercatore']."!";
       }

       if(isset($_SESSION['userSemplice'])){
         echo '<p align="left"><a href="profiloSemplice.php">Torna alla tua area personale</a></p>';
       }
       else if(isset($_SESSION['userPremium'])){
         echo '<p align="left"><a href="profiloPremium.php">Torna alla tua area personale</a></p>';
       }
       else{
         echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
       }
    ?>
    <br>
    <p align="left"><strong>Fai una donazione: </strong></p>
    <form id="FormEffettuaDonazione" action="nuovaDonazione.php" method="post" align="center">
      Username:
      <?php
        if(isset($_SESSION['userSemplice'])){
          echo $_SESSION['userSemplice'];
        }
        else if(isset($_SESSION['userPremium'])){
          echo $_SESSION['userPremium'];
        }
        else{
          echo $_SESSION['ricercatore'];
        }
      ?><br><br>
      ID raccolta fondi:<br>
      <input type="number" name="IDRaccolta"><br><br>
      Importo donato: <br>
      <input type="number" min="0" name="importoDonato"><br><br>
      Note:<br>
      <textarea name="note" rows="5" cols="40"></textarea><br><br>
      <input type="Submit" class="button button-block" value="Invia">
    </form>
    <br><br>

<div class="p-3 mb-2 bg-warning text-dark">
  <h2>Raccolte fondi presenti:</h2>
    <?php
    try {
       $sql='SELECT * FROM RACCOLTA_FONDI';
       $res=$pdo->query($sql);

       
      echo'<table class= "table table-hover table-dark">';
       echo"<tr>";
       echo'<th scope="col">';
       echo"Id:";
       echo"</th>";
       echo'<th scope="col">';
       echo"NOME RACCOLTA FONDI:";
       echo"</th>";
       echo'<th scope="col">';
       echo"MPORTO RACCOLTO:";
       echo"</th>";
       echo'<th scope="col">';
       echo"IMPORTO MASSIMO:";
       echo"</th>";
       echo'<th scope="col">';
       echo"STATO:";
       echo"</th>";
       echo"</tr>";  


       while($row=$res->fetch()) {
         echo'<tr>';
         echo"<td>".$row['id']."</td>";
         echo"<td>".$row['nome']."</td>";
         echo"<td>".$row['importoRaccolto']."</td>";
         echo"<td>".$row['importoMax']."</td>";
         
         if($row['importoRaccolto']<$row['importoMax']){
          echo "<td>".'<span style="color: green;" /> <b>APERTO</b></span>'."</td>";
          
         }else if($row['importoRaccolto']=$row['importoMax']){
          echo "<td>".'<span style="color: red;" /> <b>CHIUSO</b> </span>'."</td>";
          
         }

         echo"</tr>";
       }
        echo"</table>";

     }
     catch(PDOException $e) {
        echo("Errore esecuzione query.");
        exit();
     } ?>
</body>
</html>
