<?php
   include 'connection.php';
   //$user=$_POST["Username"];
   session_start();
   if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])){
     //echo (.$user);
     header("location:home.html");
   }

   if(isset($_SESSION['userSemplice'])){
     $username=$_SESSION['userSemplice'];
   }
   else{
     $username=$_SESSION['userPremium'];
   }
   $idesc=$_POST["idescursione"];

   try{
     $sql="SELECT id FROM ESCURSIONE WHERE id=:lab1";
     $ifid=$pdo->prepare($sql);
     $ifid->bindValue(":lab1",$idesc);
     $ifid->execute();
     $check=$ifid->rowCount();

     if($check==1){
      $sql2='SELECT nomeProfilo FROM PARTECIPAZIONE_ESCURSIONI';
      $res=$pdo->query($sql2);
      
      while($row=$res->fetch()) {
        if($row['nomeProfilo']==$username){
          echo '<script>alert("Nome utente già presente in questa escursione")
               window.location.href="StatisticheEscursioni.php";</script>';
        }
      }
      $result=$pdo->query("CALL Iscrizione( '$username', '$idesc');");
     
       if(isset($_SESSION['userSemplice'])){
         echo '<script>alert("Iscrizione effettuata")
               window.location.href="profiloSemplice.php";</script>';
       }
       else if(isset($_SESSION['userPremium'])){
         echo '<script>alert("Iscrizione effettuata")
               window.location.href="profiloPremium.php";</script>';
       }
       else{
         echo '<script>alert("Iscrizione effettuata")
               window.location.href="profiloRicercatore.php";</script>';
       }

     }
     else{
       echo '<script>alert("ID non corrisponde a nessuna escursione");
             window.location.href="FormPartecipazioneEscursione.php";</script>';
     }
   }
   catch(PDOException $e){
     echo $e->getMessage();
   }

   $pdo=null;

   // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'iscrizione escursione', 'codiceEscursione' => $idesc]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }
?>
