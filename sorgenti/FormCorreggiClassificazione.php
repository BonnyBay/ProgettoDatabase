<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Correzione di classificazione </title>
</head>
<body>
  <?php
      echo "<h3> Benvenuto ".$_SESSION['ricercatore']."!";
      echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
    ?>
  <form id="correzione" action="correggiClassificazione.php" method="post" align="center">
      Username:
      <?php echo $_SESSION['ricercatore']; ?><br><br>
      Codice:
      <input type="number" min="1" name="codice"><br><br>
      Specie:
      <select name="specie">
     <?php 
        $sql = 'SELECT nomeLatino FROM SPECIE';
        $res=$pdo->query($sql);
        while ($row =$res->fetch()){
        //echo "<option value=". $row['nome'].">" . $row['nome'] . "</option>";
        echo "<option value='".$row["nomeLatino"]."'>" . $row["nomeLatino"]. "</option>";
      }
      ?>
      </select> 
      <br>
      <br>

<!--       <input type="text" name="specie" /><br><br> -->      <input type="Submit" class="button button-block" value="Correggi"><br><br>
  </form>
    <?php
    try {
       // $sql='SELECT nomeLatino FROM SPECIE';
       // $res=$pdo->query($sql);
       $sql2='SELECT * FROM SEGNALAZIONE';
       $res2=$pdo->query($sql2);
       // echo 'Specie presenti: ';
       // while($row=$res->fetch()) {
       //   echo('<br>Nome: '.$row['nomeLatino']);
       // }
       echo '<br><br>Segnalazioni presenti:';
       while($row2=$res2->fetch()) {
         echo('<br>Utente: '.$row2['nomeUtente'].' - Codice Segnalazione: '.$row2['codice']);
       }
   }
   catch(PDOException $e) {
      echo("Errore esecuzione query.");
      exit();
   }

    ?>

</body>
</html>
