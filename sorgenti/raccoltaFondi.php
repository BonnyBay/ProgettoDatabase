<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
  $username=$_SESSION['ricercatore'];
  $nomeRaccolta=$_POST["nomeRaccolta"];
  $importo=$_POST["importo"];
  $descrizione=$_POST["descrizione"];
  $dataInizio=$_POST["dataInizio"];
  try{
    if(!empty($nomeRaccolta) and !empty($descrizione)){
      $result=$pdo->query("CALL NuovaRaccoltaFondi( '$username','$nomeRaccolta','$importo','$descrizione','$dataInizio');");
      echo '<script>alert("Nuova raccolta creata con successo!");
            window.location.href="FormRaccoltaFondi.php";
            </script>';
    }
    else{
      echo '<script>alert("Compilare tutti i campi per poter proseguire.");
            window.location.href="FormRaccoltaFondi.php";
            </script>';
    }
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  $pdo=null;

   // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'creazione raccolta fondi', 'nomeRaccolta' => $nomeRaccolta, 'importo' => $importo, 'descrizione' => $descrizione, 'dataInizio' => $dataInizio]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }
?>
