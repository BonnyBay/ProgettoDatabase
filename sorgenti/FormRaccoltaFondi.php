<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Nuova Raccolta Fondi</title>
</head>
<body>
    <?php
      echo "<h3> Benvenuto ".$_SESSION['ricercatore']."!";
      echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
    ?>
    <p align="center"><strong>Crea una raccolta fondi: </strong></p>
    <form id="FormNuovaRaccoltaFondi" action="raccoltaFondi.php" method="post" align="center">
      Username: <?php echo $_SESSION['ricercatore']; ?><br><br>
      Nome raccolta fondi:<br>
      <input type="text" name="nomeRaccolta"><br><br>
      Importo da raggiungere: <br>
      <input type="number" min="0" name="importo"><br><br>
      Descrizione:<br>
      <textarea name="descrizione" rows="5" cols="40"></textarea><br><br>
      Data di inizio:<br>
      <input type="date" name="dataInizio"><br><br>
      <input type="Submit" class="button button-block" value="Invia">
    </form>
</body>
</html>
