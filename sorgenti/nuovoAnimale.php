<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
  $username=$_SESSION['ricercatore'];
  $vulnerabilita=$_POST["vulnerabilita"];
  $annoClassificazione=$_POST["annoClassificazione"];
  $nomeLatino=$_POST["nomeLatino"];
  $nomeItaliano=$_POST["nomeItaliano"];
  $classe=$_POST["classe"];
  $linkWiki=$_POST["linkWiki"];
  $altezza=$_POST["altezza"];
  $peso=$_POST["peso"];
  $nProle=$_POST["nProle"];
  try{
    if(!empty($vulnerabilita) and !empty($annoClassificazione) and !empty($nomeLatino) and !empty($nomeItaliano) and !empty($classe) and !empty($linkWiki) and !empty($altezza) and !empty($peso) and !empty($nProle)){
      $result=$pdo->query("CALL NuovoAnimale( '$nomeLatino','$username','$vulnerabilita', '$annoClassificazione', '$nomeItaliano','$classe','$linkWiki','$peso','$altezza','$nProle');");
      echo '<script>alert("Nuovo animale inserito con successo!");
            window.location.href="FormNuovoAnimale.php";
            </script>';
    }
    else{
      echo '<script>alert("Compilare tutti i campi per poter proseguire.");
            window.location.href="FormNuovoAnimale.php";
            </script>';
    }
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  $pdo=null;

   // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'inserimento nuovo animale', 'nomeLatino' => $nomeLatino]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }
?>
