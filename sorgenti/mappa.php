<?php
include 'connection.php';
session_start();
if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium']) and !isset($_SESSION['ricercatore'])){
 //echo (.$user);
 header("location:login.html");
}
try {
     $sql="SELECT * from SEGNALAZIONE,SPECIE where (nomeSpecie = any(SELECT nomeLatino from ANIMALE))and(nomeSpecie=nomeLatino)";
     $res=$pdo->query($sql);
     $righe = $res->rowCount();
     $pointAnimale=$res->fetchAll();
     } catch(PDOException	$e)	{
              echo("Codice errore".$e->getMessage()."<br>");
	      exit();
    }

try {
     $sql="SELECT * from SEGNALAZIONE,SPECIE where (nomeSpecie = any(SELECT nomeLatino from VEGETALE)) and (nomeSpecie=nomeLatino) ";
     $res=$pdo->query($sql);
     $righe = $res->rowCount();
     $pointVegetale=$res->fetchAll();
     } catch(PDOException $e) {
              echo("Codice errore".$e->getMessage()."<br>");
        exit();
    }
    
try {
     $sql="SELECT * from SEGNALAZIONE where nomeSpecie is NULL";
     $res=$pdo->query($sql);
     $righe = $res->rowCount();
     $point=$res->fetchAll();
     } catch(PDOException $e) {
              echo("Codice errore".$e->getMessage()."<br>");
        exit();
    }
?>

<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<title>Mappa Segnalazioni</title>
<link rel="stylesheet" type="text/css" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
 <script type='text/javascript' src='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js'></script>

</head>

<body>
<div class="p-3 mb-2 bg-warning text-dark"><p align="left"><strong><h1>Mappa delle segnalazioni</h1></strong></p></div>
<div id="map" style="height: 600px; width: 100%; border: 1px solid #AAA;"></div>

<p id="demo"></p>

<script>

var map = L.map( 'map', {
    center: [20.0, 5.0],
    minZoom: 2,
    zoom: 2
});


L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a','b','c']
}).addTo( map );

var valoriAnimale = <?php echo JSON_encode($pointAnimale);?>;
var valoriVegetale = <?php echo JSON_encode($pointVegetale);?>;
var valori = <?php echo JSON_encode($point);?>;

var greenIcon = L.icon({
    iconUrl: 'foglia.png',
    //shadowUrl: 'leaf-shadow.png',

    iconSize:     [20, 70], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [15, 70], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});
for (var i = 0; i < valoriVegetale.length; i++)
    {
       var markerV= L.marker([Number(valoriVegetale[i].latitudine), Number(valoriVegetale[i].longitudine)],{icon: greenIcon}).addTo(map);
       markerV.bindPopup("<b>"+valoriVegetale[i].nomeItaliano+"</b><br>"+valoriVegetale[i].nomeSpecie+"</b><br>"+valoriVegetale[i].data).openPopup();
    }
var redIcon = L.icon({
    iconUrl: 'animale.png',
    //shadowUrl: 'leaf-shadow.png',

    iconSize:     [15, 25], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

for (var i = 0; i < valoriAnimale.length; i++)
    {
       var markerA= L.marker([Number(valoriAnimale[i].latitudine), Number(valoriAnimale[i].longitudine)],{icon: redIcon}).addTo(map);
       markerA.bindPopup("<b>"+valoriAnimale[i].nomeItaliano+"</b><br>"+valoriAnimale[i].nomeSpecie+"</b><br>"+valoriAnimale[i].data).openPopup();
    }

for (var i = 0; i < valori.length; i++)
    {
       var markerQ= L.marker([Number(valori[i].latitudine), Number(valori[i].longitudine)]).addTo(map);
       markerQ.bindPopup("<b> Specie sconosciuta</b><br>"+valori[i].data).openPopup();
    }


</script>
<div class="container">
  <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4">
    <?php  
    if(isset($_SESSION['userSemplice'])){
    echo '<form action= "profiloSemplice.php">';
  }
  else if(isset($_SESSION['userPremium'])){
    echo '<form action= "profiloPremium.php">';
  } 
  else {
    echo '<form action= "profiloRicercatore.php">';
  } 

  ?>
  <button class="btn btn-primary btn-block" type="submit">Torna al profilo</button>
</form>
</div>
</div>
</body>
</html>
