<?php
 include 'connection.php';
 //$user=$_POST["Username"];
 session_start();
 if(!isset($_SESSION['userPremium'])){
   //echo (.$user);
   header("location:home.html");
 }
?>
<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="Web2.css">
   <title>Il tuo profilo</title>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <p class="navbar-brand">
    <?php 
    $username=$_SESSION['userPremium'];
    $sql="SELECT tipoAccount FROM PROFILO WHERE nome='$username'";
    $res=$pdo->query($sql);
    while($row=$res->fetch()) {
      if($row['tipoAccount']='UTENTE PREMIUM'){
        echo('<span style="color: #ffc61a; background-color: #664d00"; />'.$row['tipoAccount'].'&nbsp&nbsp&nbsp&nbsp&nbsp'); 
      }
    } ?>
    </p>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">

          <form action="FormAggiornaProfilo.php" class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Aggiorna Profilo</button>
            </li>
          </form>

        <form action="FormMessaggi.php" class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Messaggi</button>
            </li>
        </form>

        <form action="FormVisualizzaStatistiche.php" class="form-inline my-2 my-lg-0">   
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Visualizza Statistiche</button>
          </li>
        </form>

        <form action="mappa.php" class="form-inline my-2 my-lg-0">   
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Mappa Segnalazioni</button>
          </li>
        </form>

      </ul>
      <form action="visualizzaDatiProfilo.php" method="post" class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Cerca un utente" aria-label="Search"  name="userCercato">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cerca</button>
      </form>
      </div>
      </nav>
   <?php
     echo "<h3> Benvenuto ".$_SESSION['userPremium']."!";
   ?><br>
   <br>
   <?php
     $user=$_SESSION['userPremium'];
     $res=$pdo->query("SELECT foto FROM PROFILO WHERE nome='$user'");
     $result=$res->fetch();
     if(base64_encode(base64_decode($result["foto"])) == $result["foto"] and $result["foto"]!=""){
       $foto='<img  height="200px" width="200px" src=data:image/png;base64,'.$result["foto"].'>'."<br>";
     }
     else{
       $foto='<img  height="200px" width="200px" src=nofoto.png /><br>';
     }
     echo $foto;
   ?>
   <h4><a href="logout.php">Logout</a></h4><br>
   <h3 align="center">Che cosa vuoi fare?</h3>
   <form action="FormAggiungiProposta.php" align="center">
     <p>Aggiungi una nuova proposta: </p>
     <input type="Submit" class="button button-block" value="Continua" id="PulsanteDiAvvio">
   </form><br>
   <form action="FormNuovaSegnalazione.php" align="center">
     <p>Fai una segnalazione: </p>
     <input type="Submit" class="button button-block" value="Continua" id="PulsanteDiAvvio">
   </form><br>
   <form action="FormCreaEscursione.php" align="center">
     <p>Crea una tua escursione:</p>
     <input type="Submit" class="button button-block" value="Continua" id="PulsanteDiAvvio">
   </form><br>
   <form action="FormPartecipazioneEscursione.php" align="center">
     <p>Iscriviti ad una escursione: </p>
     <input type="Submit" class="button button-block" value="Continua" id="PulsanteDiAvvio">
   </form><br>
    <form action="FormEffettuaDonazione.php" align="center">
      <p>Effettua una donazione verso una raccolta fondi:</p>
      <input type="Submit" class="button button-block" value="Continua" id="PulsanteDiAvvio">
    </form>

</body>
</html>
