<?php
   include 'connection.php';
   require 'FormNuovaSegnalazione.php';
   //$user=$_POST["Username"];
   session_start();
   if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])){
     //echo (.$user);
     header("location:home.html");
   }
   if(isset($_SESSION['userSemplice'])){
     $username=$_SESSION['userSemplice'];
   }
   else{
     $username=$_SESSION['userPremium'];
   }
  $habitat=$_POST["nomeHabitat"];
  $lat=$_POST["latitudine"];
  $long=$_POST["longitudine"];
  $img=$_FILES['foto']['tmp_name'];
  $foto = base64_encode(file_get_contents(addslashes($img)));

  //***************** DATA MINING *****************
  //
  $sql='SELECT count(*) as c FROM SEGNALAZIONE';
  $res=$pdo->query($sql);
  $max = $res->fetch();
  $m=$max['c'];

   $sql='SELECT * FROM SEGNALAZIONE';
   $res2=$pdo->query($sql);
   while($row3=$res2->fetch()) {
     $nomeHabitat[]=$row3['nomeHabitat'];
     $latitudine[]=$row3['latitudine'];
     $longitudine[]=$row3['longitudine'];
     $nomeSpecie[]=$row3['nomeSpecie'];
   }

   $i=0;

   while($i<$m){
     $samples[] = [$nomeHabitat[$i], $latitudine[$i], $longitudine[$i]];
     $labels[] = $nomeSpecie[$i];
     $i=$i+1;
   }

  require_once __DIR__ . '/vendor/autoload.php';
  use Phpml\Classification\NaiveBayes;
  //training set
  $classifier = new NaiveBayes();
  $classifier->train($samples, $labels);

  $speciePredetta=$classifier->predict([$habitat, $lat, $long]);


  try{
    $sql="SELECT nome FROM HABITAT WHERE nome=:lab1";
    $ifhabitat=$pdo->prepare($sql);
    $ifhabitat->bindValue(":lab1",$habitat);
    $ifhabitat->execute();
    $check=$ifhabitat->rowCount();
    if($check==1){
      if(!empty($lat) and !empty($long) and !empty($foto)){
      $result=$pdo->query("CALL NuovaSegnalazione( '$username', '$habitat', '$lat', '$long', '$foto');");
      //echo $_SESSION['specieFutura'];
      // echo "<script>alert('$_SESSION['specieFutura']');</script>";
      // echo '<script>alert("La prossima specie inserita potrebbe essere:");
      //       window.location.href="FormNuovaSegnalazione.php";
      //       </script>';
      echo '<script>alert("Segnalazione inserita con successo!\n\nSpecie Predetta:'.$speciePredetta.'");
            window.location.href="FormNuovaSegnalazione.php";
            </script>';
      }
      else{
        echo'<script>alert("Inserire anche latitutdine,longitudine e una foto");
              window.location.href="FormNuovaSegnalazione.php";
              </script>';
      }
    }
    else{
      echo '<script>alert("Habitat inserito non è presente nel database");
            window.location.href="FormNuovaSegnalazione.php";
            </script>';
    }
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  $pdo=null;

   // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'inserimento segnalazione', 'habitat' => $habitat, 'lat' => $lat, 'long' => $long]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }
?>
