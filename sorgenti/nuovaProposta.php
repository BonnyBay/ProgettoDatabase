<?php
   include 'connection.php';
   //$user=$_POST["Username"];
   session_start();
   if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])){
     //echo (.$user);
     header("location:home.html");
   }

   if(isset($_SESSION['userSemplice'])){
     $username=$_SESSION['userSemplice'];
   }
   else{
     $username=$_SESSION['userPremium'];
   }
  $codice=$_POST["codice"];
  $specie=$_POST["specie"];
  $commento=$_POST["commento"];

  try{
      $sql="SELECT nomeLatino FROM SPECIE WHERE nomeLatino=:lab1";
      $ifspecie=$pdo->prepare($sql);
      $ifspecie->bindValue(":lab1",$specie);
      $ifspecie->execute();
      $check1=$ifspecie->rowCount();
      if($check1==1){
        $sql="SELECT codice FROM SEGNALAZIONE WHERE codice=:lab2";
        $ifcodice=$pdo->prepare($sql);
        $ifcodice->bindValue(":lab2",$codice, PDO::PARAM_STR);
        $ifcodice->execute();
        $check2=$ifcodice->rowCount();
        if($check2==1){
          $result=$pdo->query("CALL NuovaProposta( '$username', '$codice', '$specie', '$commento');");
          echo '<script>alert("Proposta inserita correttamente");
                window.location.href="FormAggiungiProposta.php";
                </script>';
        }
        else{
          echo '<script>alert("Il codice non corrisponde a nessuna segnalazione");
          window.location.href="FormAggiungiProposta.php";
          </script>';
        }
      }
      else{
        echo '<script>alert("La specie non è presente nel database");
        window.location.href="FormAggiungiProposta.php";
        </script>';
      }
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  $pdo=null;

   // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'inserimento proposta', 'codiceSegnalazione' => $codice, 'specie' => $specie]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }
?>
