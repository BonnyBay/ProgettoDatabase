 <?php
  include 'connection.php';
  //$user=$_POST["Username"];
  session_start();
  if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])){
    //echo (.$user);
    header("location:home.html");
  }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="Web1.css">
    <title>Il tuo profilo</title>
</head>
<body>
  <?php
  if(isset($_SESSION['userSemplice'])){
    echo "<h3> Benvenuto ".$_SESSION['userSemplice']."!";
  }
  else{
    echo "<h3> Benvenuto ".$_SESSION['userPremium']."!";
  }
  if(isset($_SESSION['userSemplice'])){
    echo '<p align="left"><a href="profiloSemplice.php">Torna alla tua area personale</a></p>';
  }
  else{
    echo '<p align="left"><a href="profiloPremium.php">Torna alla tua area personale</a></p>';
  }
  ?>
    <p align="center"><strong>Aggiungi una nuova proposta: </strong></p>
    <form id="FormProposta" action="nuovaProposta.php" method="post" align="center">
      <?php
        if(isset($_SESSION['userSemplice'])){
          echo "<h3> Username: ".$_SESSION['userSemplice']."<br><br>";
        }
        else{
          echo "<h3> Username: ".$_SESSION['userPremium']."<br><br>";
        }
      ?>
      Codice:
      <input type="number" min="1" name="codice"><br><br>

      Specie:
      <select name="specie">
     <?php
        $sql = 'SELECT nomeLatino FROM SPECIE';
        $res=$pdo->query($sql);
        while ($row =$res->fetch()){
        //echo "<option value=". $row['nome'].">" . $row['nome'] . "</option>";
        echo "<option value='".$row["nomeLatino"]."'>" . $row["nomeLatino"]. "</option>";
      }
      ?>
      </select>
      <br>
      <br>
<!--       <input type="text" name="specie" />-->


      Commento:<br>
       <textarea class="form-control" name="commento" id="exampleFormControlTextarea1" rows="3" ></textarea>
<!--       <textarea name="commento" rows="5" cols="40"></textarea><br><br> -->
      <input type="Submit" class="button button-block" value="Invia proposta"><br><br>
    </form>
<!--     Specie vegetali disponibili:-->
 <div class="p-3 mb-2 bg-warning text-dark">
  <h2>Segnalazioni Presenti</h2>
    <?php
    try {
       // $sql='SELECT nomeLatino FROM SPECIE';
       // $res=$pdo->query($sql);
       $sql2='SELECT * FROM SEGNALAZIONE';
       $res2=$pdo->query($sql2);
       // while($row=$res->fetch()) {
       //   echo('<br>Nome: '.$row['nomeLatino']);
       // }
       echo'<table class= "table table-hover table-dark">';
       echo"<tr>";
       echo'<th scope="col">';
       echo"Utente";
       echo"</th>";
       echo'<th scope="col">';
       echo"Codice Segnalazione:";
       echo"</th>";
       echo'<th scope="col">';
       echo"Nome Habitat:";
       echo"</th>";
       echo'<th scope="col">';
       echo"Latitudine:";
       echo"</th>";
       echo'<th scope="col">';
       echo"Longitudine:";
       echo"</th>";
       echo'<th scope="col">';
       echo"Foto:";
       echo"</th>";
       echo"</tr>";

       while($row2=$res2->fetch()) {
         if(base64_encode(base64_decode($row2["foto"])) == $row2["foto"]){
           $foto='<img  height="100px" width="100px" src=data:image/png;base64,'.$row2["foto"].'>'."<br>";
         }
         else{
           $foto="";
         }

         echo'<form action="visualizzaFoto.php" method="post"><tr>';
         echo"<td>".$row2['nomeUtente']."</td>";
         echo"<td>".$row2['codice'].'<input type="hidden" name="codSegn" value="'.$row2['codice'].'"/></td>';
         echo"<td>".$row2['nomeHabitat']."</td>";
         echo"<td>".$row2['latitudine']."</td>";
         echo"<td>".$row2['longitudine']."</td>";
         echo'<td><a href="visualizzaFoto.php?cs='.$row2['codice'].'" >Visualizza</a></td>';
         echo"</tr>";

       }
       echo"</table>";
   }
   catch(PDOException $e) {
      echo("Errore esecuzione query.");
      exit();
   }


    ?>

</body>
</html>
