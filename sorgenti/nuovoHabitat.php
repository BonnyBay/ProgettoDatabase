<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
  $username=$_SESSION['ricercatore'];
  $nome=$_POST["nomeHabitat"];
  $descrizione=$_POST["descrizione"];
  try{
    if(!empty($nome) and !empty($descrizione)){
      $result=$pdo->query("CALL InserimentoHabitat( '$nome','$username','$descrizione');");
      echo '<script>alert("Nuovo habitat inserito con successo!");
            window.location.href="FormNuovoHabitat.php";
            </script>';
    }
    else{
      echo '<script>alert("Compilare tutti i campi per poter proseguire.");
            window.location.href="FormNuovoHabitat.php";
            </script>';
    }
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  $pdo=null;

   // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'inserimento nuovo habitat', 'nomeHabitat' => $nome]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }

?>
