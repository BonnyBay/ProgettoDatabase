<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
  $username=$_SESSION['ricercatore'];
  $nomeHabitat=$_POST['nomeHabitat'];
  try{
    if(!empty($nomeHabitat)){
      $sql="SELECT nome FROM HABITAT WHERE nome=:lab1";
      $ifHabitat=$pdo->prepare($sql);
      $ifHabitat->bindValue(":lab1",$nomeHabitat);
      $ifHabitat->execute();
      $check=$ifHabitat->rowCount();
      if($check==1){
        $result=$pdo->query("CALL EliminaHabitat('$username','$nomeHabitat');");
        echo '<script>alert("Habitat eliminato con successo!");
              window.location.href="FormEliminaHabitat.php";
              </script>';
      }else{
        echo '<script>alert("Habitat non presente nel database!");
              window.location.href="FormEliminaHabitat.php";
              </script>';
      }
    }
    else{
      echo '<script>alert("Compilare tutti i campi per poter proseguire.");
            window.location.href="FormEliminaHabitat.php";
            </script>';
    }
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }

  $pdo=null;

  // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'elimina habitat', 'nomeHabitat' => $nomeHabitat]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }

?>
