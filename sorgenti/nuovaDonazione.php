<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium'])and !isset($_SESSION['ricercatore'])){
     //echo (.$user);
     header("location:home.html");
   }
  if(isset($_SESSION['userSemplice'])){
     $username=$_SESSION['userSemplice'];
   }
  else if(isset($_SESSION['userPremium'])){
     $username=$_SESSION['userPremium'];
   }else{
    $username=$_SESSION['ricercatore'];
   }
  $IDRaccolta=$_POST["IDRaccolta"];
  $importoDonato=$_POST["importoDonato"];
  $note=$_POST["note"];
  try{

   $sql="SELECT * FROM RACCOLTA_FONDI WHERE id='$IDRaccolta'";
   $res=$pdo->query($sql);
   $count = $res->rowCount();
   if ($count > 0){
      while($row=$res->fetch()) {
           if($row['stato']=="chiuso"){
            echo '<script>alert("Non è possibile effettuare altre donazioni. È stato raggiunto il massimo importo. ");
             window.location.href="FormEffettuaDonazione.php";</script>';
           }else if($row['importoMax']<$importoDonato){
             echo '<script>alert("Importo donato superiore ad importo massimo donabile. Non è possibile effettuare la donazione. ");
             window.location.href="FormEffettuaDonazione.php";</script>';
           }
      }
     if(!empty($username) and !empty($importoDonato) ){
       $result=$pdo->query("CALL NuovaDonazione( '$username','$importoDonato','$note', '$IDRaccolta');");
       if(isset($_SESSION['userSemplice'])){
         echo '<script>alert("Donazione effettuata con successo!");
               window.location.href="profiloSemplice.php";
               </script>';
       }
       else if(isset($_SESSION['userPremium'])) {
         echo '<script>alert("Donazione effettuata con successo!");
               window.location.href="profiloPremium.php";
               </script>';
       }else{
          echo '<script>alert("Donazione effettuata con successo!");
               window.location.href="profiloRicercatore.php";
               </script>';
       }
     }
     else{
       echo '<script>alert("La donazione non è andata a buon fine");
             window.location.href="FormEffettuaDonazione.php";</script>';
     }
 }else{
    echo '<script>alert("La donazione non è andata a buon fine");
             window.location.href="FormEffettuaDonazione.php";</script>';
 }

 }
 catch(PDOException $e){
   echo $e->getMessage();
 }

  $pdo=null;

  // inserimento nel log
    try {
     require '/Applications/MAMP/bin/php/php7.3.8/bin/vendor/autoload.php';
     $client = new MongoDB\Client("mongodb://127.0.0.1:27017");
     $collection = $client -> ProgettoDB -> Log;
     $collection -> insertOne(['data' => date("F j, Y, g:i a"), 'utente' => $username, 'azione' => 'inserimento donazione', 'idRaccolta' => $IDRaccolta, 'importoDonato' => $importoDonato]);
   } catch (MongoDB\Client\Exception\Exception $e) {
    echo("Errore: ".$e->getMessage()."<br>");
  }

?>
