<?php
 include 'connection.php';
 //$user=$_POST["Username"];
 session_start();
 if( (!isset($_SESSION['userSemplice'])) and (!isset($_SESSION['userPremium'])) and (!isset($_SESSION['ricercatore'])) ){
   //echo (.$user);
   header("location:home.html");
 }
?>

<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="Web2.css">
   <title>Statistiche</title>
</head>
<body>
 <div class="p-3 mb-2 bg-warning text-dark"><p align="left"><strong><h1>Statistiche</h1></strong></p></div>
 <hr>
 <?php
   if(isset($_SESSION['userSemplice'])){
     echo "<h3> Ciao ".$_SESSION['userSemplice']."!";
   }
   else if(isset($_SESSION['userPremium'])){
     echo "<h3> Ciao ".$_SESSION['userPremium']."!";
   }
   else{
     echo "<h3> Ciao ".$_SESSION['ricercatore']."!";
   }
 ?>
   <div class="container">
   <form id="statistiche" action="statistiche.php" method="post" align="center" >
     <br>Utenti attivi:
     <input type="Submit" class="btn btn-primary btn-lg" value="continua">
   </form>
   <form id="statistichePremium" action="StatistichePremium.php" method="post" align="center" >
     <br>Utenti Premium:
     <input type="Submit" class="btn btn-primary btn-lg" value="continua">
   </form>
   <form id="EscursioniInserite" action="StatisticheEscursioni.php" method="post" align="center" >
     <br>Escursioni inserite:
     <input type="Submit" class="btn btn-primary btn-lg" value="continua">
   </form>
   <form id="CampagnaFondi" action="StatisticheCampagneFondi.php" method="post" align="center" >
     <br>Campagne Fondi:
     <input type="Submit" class="btn btn-primary btn-lg" value="continua">
   </form>
   <form id="ClassificaSpecie" action="classificaSpecie.php" method="post" align="center" >
     <br>Classifica specie:
     <input type="Submit" class="btn btn-primary btn-lg" value="continua">
   </form>
   </div>
   <br>
   <?php
   if(isset($_SESSION['userSemplice'])){
     echo '<p align="left"><a href="profiloSemplice.php">Torna alla tua area personale</a></p>';
   }
   else if(isset($_SESSION['userPremium'])){
     echo '<p align="left"><a href="profiloPremium.php">Torna alla tua area personale</a></p>';
   }
   else{
     echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
   }
   ?>
</body>
</html>
