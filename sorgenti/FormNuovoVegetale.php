<?php
   //sessioni da 24 min
  session_start();
  if(!isset($_SESSION['ricercatore'])){
    header("location:home.html");
  }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Il tuo profilo</title>
</head>
<body>
    <?php
      echo "<h3> Benvenuto ".$_SESSION['ricercatore']."!";
      echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
    ?>
    <p align="center"><strong>Aggiungi un nuovo vegetale: </strong></p>
    <form id="FormVegetale" action="nuovoVegetale.php" method="post" align="center">
      Username: <?php echo $_SESSION['ricercatore']; ?><br><br>
      Livello vulnerabilità:<br>
      <div>
        <label>
          <input type="radio" class="radio" value="Alto" name="vulnerabilita" />Alto
        </label>
        <label>
          <input type="radio" class="radio" value="Medio" name="vulnerabilita" />Medio
        </label>
        <label>
          <input type="radio" class="radio" value="Basso" name="vulnerabilita" />Basso
        </label> 
      </div>
      <br>
      Anno di classificazione:<br>
      <input type="number" name="annoClassificazione"><br><br>
      Nome latino del vegetale:<br>
      <input type="text" name="nomeLatino"><br><br>
      Nome italiano del vegetale:<br>
      <input type="text" name="nomeItaliano"><br><br>
      Classe:<br>
      <input type="text" name="classe"><br><br>
      Link alla pagina di wikipedia:<br>
      <input type="text" name="linkWiki"><br><br>
      Altezza: <br>
      <input type="number" min="0" name="altezza"><br><br>
      Diametro: <br>
      <input type="number" min="0" name="diametro"><br><br>

      <input type="Submit" class="button button-block" value="Invia">
    </form>
    <?php //session_destroy() ?>
</body>
</html>
