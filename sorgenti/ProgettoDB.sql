drop DATABASE if exists ProgettoDB_Nature;
CREATE DATABASE ProgettoDB_Nature;
USE ProgettoDB_Nature;

/*  MESSAGGIO (id, mittente, destinatario, timestamp, testo, titolo)  */
CREATE TABLE MESSAGGIO (
  id int auto_increment primary key,
  mittente varchar (30) references PROFILO (nome),
  destinatario varchar (30) references PROFILO (nome),
  timeStamp timeStamp,
  testo varchar (280),
  titolo varchar (30)

) ENGINE=InnoDB;


/* tabella che accorpa la generalizzazione in PROFILO
PROFILO (nome, tipoAccount, email, foto, professione, annoNascita, password,
dataRegistrazione, nSegnalazioni, nCorrette, nErrate, affidabilita)*/
CREATE TABLE PROFILO (
  nome varchar (30) primary key,
  tipoAccount enum ('UTENTE SEMPLICE', 'UTENTE PREMIUM', 'RICERCATORE'),
  email varchar (30),
  password varchar (32),
  foto longblob,
  professione varchar (30),
  annoNascita int,
  nSegnalazioni int default 0,
  dataRegistrazione timeStamp default current_timestamp ,

  nCorrette int default 0,
  nErrate int default 0
) ENGINE=InnoDB;


/* HABITAT (nome, nomeRicercatore, descrizione) */
CREATE TABLE HABITAT (
  nome varchar (50) primary key,
  nomeRicercatore varchar (30) references PROFILO (nome),
  descrizione varchar (280)

) ENGINE=InnoDB;


/*  SPECIE (nomeLatino, nomeRicercatore,
 livVulnerabilita, annoClassificazione, nomeItaliano, classe, linkWiki)*/
CREATE TABLE SPECIE (
  nomeLatino varchar (30) primary key,
  nomeRicercatore varchar(50) references PROFILO (nome),
  livVulnerabilità varchar(30),
  annoClassificazione int,
  nomeItaliano varchar (30),
  classe varchar (30),
  linkWiki varchar (90)

) ENGINE=InnoDB;


/* VEGETALE (nomeLatino, altezza, diametro)  */
CREATE TABLE VEGETALE (
  nomeLatino varchar (30) primary key references SPECIE (nomeLatino),
  altezza double,
  diametro double

) ENGINE=InnoDB;


/*ANIMALE (nomeLatino, peso, altezza, nProle)*/
CREATE TABLE ANIMALE (
  nomeLatino varchar (30) primary key references SPECIE (nomeLatino),
  peso double,
  altezza double,
  nProle int

) ENGINE=InnoDB;


/*SEGNALAZIONE (nomeUtente, codice,  nomeHabitat, nomeSpecie,
latitudine, longitudine, foto, data) */
CREATE TABLE SEGNALAZIONE (
  nomeUtente varchar (30) references PROFILO (nome),
  codice int auto_increment primary key,
  nomeHabitat varchar (50) references HABITAT (nome),
  nomeSpecie varchar (50)references SPECIE (nomeLatino),
  latitudine  double,
  longitudine  double,
  foto longblob,
  data date

) ENGINE=InnoDB;


/* PROPOSTA (id, nomeUtente, codiceSegnalazione, specie, data, commento)*/
CREATE TABLE PROPOSTA (
  id int auto_increment primary key,
  nomeUtente varchar (30) references PROFILO (nome),
  codiceSegnalazione int references SEGNALAZIONE (codice),
  specie varchar (30) references SPECIE (nomeLatino),
  data date,
  commento varchar (280)

) ENGINE=InnoDB;


/* ESCURSIONE (id, nomeCreatore, data, titolo,
tragitto, nPartecipanti, descrizione, orarioPartenza, orarioRitorno)*/
CREATE TABLE ESCURSIONE (
  id int auto_increment primary key,
  nomeCreatore varchar (30) references PROFILO (nome),
  data date,
  titolo varchar (30),
  tragitto text,
  nPartecipanti int,
  descrizione text,
  orarioPartenza time,
  orarioRitorno time

) ENGINE=InnoDB;


/*RACCOLTA_FONDI (id, nomeCreatore, stato, importoMax, descrizione, dataInizio)*/
CREATE TABLE RACCOLTA_FONDI (
  id int auto_increment primary key,
  nomeCreatore varchar (30) references PROFILO (nome),
  nome varchar(50),
  stato enum ('aperto', 'chiuso'),
  importoRaccolto double default 0,
  importoMax double,
  descrizione text,
  dataInizio date

) ENGINE=InnoDB;


/*DONAZIONE (id, nomeDonatore, idRaccolta, note, importo)*/
CREATE TABLE DONAZIONE (
  id int auto_increment primary key,
  nomeDonatore varchar (30) references PROFILO (nome),
  idRaccolta int references RACCOLTA_FONDI(id),
  note varchar(280),
  importo double

) ENGINE=InnoDB;


/*PARTECIPAZIONE_ESCURSIONI (idEscursione, nomeProfilo)*/
CREATE TABLE PARTECIPAZIONE_ESCURSIONI (
  idEscursione int references ESCURSIONE (id),
  nomeProfilo varchar (30) references PROFILO (nome),

  PRIMARY KEY (idEscursione, nomeProfilo)
) ENGINE=InnoDB;


/*APPARTENENZAH (nomeSpecie, nomeHabitat)*/
CREATE TABLE APPARTENENZAH (
  nomeSpecie varchar (30) references SPECIE (nomeLatino) on delete cascade,
  nomeHabitat varchar (50) references HABITAT (nome) on delete cascade,

  PRIMARY KEY (nomeSpecie, nomeHabitat)
) ENGINE=InnoDB;

# ------  TRIGGERS ----------

delimiter |

|

create view conteggioProposte (codiceSegnalazione, specie, totVoti) as (
select codiceSegnalazione, specie, count(*) as totVoti
from PROPOSTA
group by codiceSegnalazione, specie
);


# classificazione maggioranza assoluta
|

create trigger Classificazione
after insert on PROPOSTA
for each row
begin

declare specieVincente varchar(30);
declare h varchar(50);

if (( select count(*) from PROPOSTA where codiceSegnalazione = new.codiceSegnalazione) > 4
      and
      1 = (select count(*) from conteggioProposte where codiceSegnalazione=new.codiceSegnalazione and
                                                       totVoti = (  select max(totVoti)
                                                                    from conteggioProposte
                                                                    where codiceSegnalazione=new.codiceSegnalazione))
      and
      (select nomeSpecie from SEGNALAZIONE where codice=new.codiceSegnalazione) is NULL
    ) then
  select specie into specieVincente
  from conteggioProposte
  where codiceSegnalazione = new.codiceSegnalazione and totVoti = ( select max(totVoti)
                                                                    from conteggioProposte
                                                                    where codiceSegnalazione=new.codiceSegnalazione);
  update SEGNALAZIONE set nomeSpecie = specieVincente where codice = new.codiceSegnalazione;

  select nomeHabitat into h from SEGNALAZIONE where codice=new.codiceSegnalazione;
    if (specieVincente <> all(select nomeSpecie from APPARTENENZAH where nomeHabitat = h)) then
    insert into APPARTENENZAH values (specieVincente, h);
    end if;
  end if;
end;
|

#classificazione maggioranza relativa
/*|

create trigger Classificazione  
after insert on PROPOSTA
for each row
begin

declare specieVincente varchar(30);
declare h varchar(50);

if ((select count(*) from PROPOSTA where codiceSegnalazione = new.codiceSegnalazione) = 5) then
  select specie into specieVincente
  from conteggioProposte
  where codiceSegnalazione = new.codiceSegnalazione and totVoti = ( select max(totVoti)
                                                                    from conteggioProposte
                                                                    where codiceSegnalazione=new.codiceSegnalazione);
  update SEGNALAZIONE set nomeSpecie = specieVincente where codice = new.codiceSegnalazione;

  select nomeHabitat into h from SEGNALAZIONE where codice=new.codiceSegnalazione;

  insert into APPARTENENZAH values (specieVincente, h);
  end if;
end;
|*/

|

CREATE TRIGGER Promozione
  AFTER INSERT ON SEGNALAZIONE
  FOR EACH ROW
BEGIN
  # ogni volta che viene inserita una SEGNALAZIONE nSegnalazioni viene aggiornato
  UPDATE PROFILO SET nSegnalazioni = ( nSegnalazioni + 1 ) WHERE ( nome = NEW.nomeUtente );
  # se è stata inserita la terza SEGNALAZIONE dallo stesso utente
  if (( SELECT nSegnalazioni
      FROM PROFILO
      WHERE nome = NEW.nomeUtente ) = 3 )
  THEN
  # allora viene promosso ad utente premium
   UPDATE PROFILO SET tipoAccount = 'UTENTE PREMIUM' WHERE ( nome = NEW.nomeUtente );
  END if;
END;
|

|

CREATE TRIGGER ChiudiRaccolta
  AFTER INSERT ON DONAZIONE
  FOR EACH ROW
BEGIN
# se supero o eguaglio la soglia chiudo la RACCOLTA_FONDI
  if (( SELECT importoRaccolto
   FROM RACCOLTA_FONDI
      WHERE id = new.idRaccolta ) + new.importo ) >= ALL (  SELECT importoMax
                    FROM RACCOLTA_FONDI
                    WHERE id = new.idRaccolta )
  THEN
   UPDATE RACCOLTA_FONDI SET stato = 'chiuso' WHERE id = new.idRaccolta;
  END if;
END;
|

|

create trigger AggiornaStatistiche
  after update on SEGNALAZIONE
  for each row
begin
# verifico che il nome della specie aggiornato nella SEGNALAZIONE sia uguale a quello della PROPOSTA che fa riferimento
# alla SEGNALAZIONE modificata

update PROFILO set nCorrette = ( nCorrette + 1 )
  where nome in ( select PROPOSTA.nomeUtente
                  from SEGNALAZIONE, PROPOSTA
                  where new.codice = codiceSegnalazione and
                        new.nomeSpecie = PROPOSTA.specie and
                        (new.nomeSpecie <> old.nomeSpecie or old.nomeSpecie is null));

update PROFILO set nErrate = (nErrate + 1 )
  where nome in ( select PROPOSTA.nomeUtente
                  from SEGNALAZIONE, PROPOSTA
                  where new.codice = codiceSegnalazione and
                        new.nomeSpecie <> PROPOSTA.specie and
                        (new.nomeSpecie <> old.nomeSpecie or old.nomeSpecie is null));

update PROFILO set nCorrette = ( nCorrette - 1 )
  where nome in ( select PROPOSTA.nomeUtente
                  from SEGNALAZIONE, PROPOSTA
                  where new.codice = codiceSegnalazione and
                        new.nomeSpecie <> old.nomeSpecie and
                        new.nomeSpecie <> PROPOSTA.specie and
                        old.nomeSpecie = PROPOSTA.specie);

update PROFILO set nErrate = ( nErrate - 1 )
  where nome in ( select PROPOSTA.nomeUtente
                  from SEGNALAZIONE, PROPOSTA
                  where new.codice = codiceSegnalazione and
                        ((new.nomeSpecie <> old.nomeSpecie and new.nomeSpecie = PROPOSTA.specie) or
                        (new.nomeSpecie <> old.nomeSpecie and old.nomeSpecie <> PROPOSTA.specie)) and
                        (old.nomeSpecie <> PROPOSTA.specie or old.nomeSpecie is not null));
end;

|

# ------  STORED PROCEDURES ----------

# autocommit viene impostato a 0 per poter poter usare le transazioni
SET AUTOCOMMIT = 0;

|
CREATE PROCEDURE printf(mytext TEXT)
BEGIN
  select mytext as ``;
END;
|

|
CREATE PROCEDURE InserimentoEscursione
(IN NomeCreatore varchar(30), IN Titolo varchar(50),IN Data date,IN OrarioPartenza time,
 IN OrarioRitorno time, IN Tragitto text, IN Descrizione text, IN NPartecipanti int )
BEGIN


   #Seleziono i nomi dei profili di tipo premium e guardo se il nome inserito (nomeCreatore) coincide ad uno di questi
   IF (NomeCreatore = any (SELECT nome
                  FROM PROFILO
                  WHERE (tipoAccount = "UTENTE PREMIUM") ) ) THEN
    start transaction;
    INSERT INTO ESCURSIONE(nomeCreatore,titolo,data,orarioPartenza,orarioRitorno,tragitto,descrizione,nPartecipanti)
    VALUES(NomeCreatore,Titolo,Data,OrarioPartenza,OrarioRitorno,Tragitto,Descrizione,NPartecipanti);
    commit;
   ELSE
    CALL printf("[ERRORE]L'escursione non è stata creata!");
   END IF;
END;
|

|
CREATE PROCEDURE InserimentoHabitat
(IN Nome varchar (50),IN nomeUtente varchar (30),IN Descrizione varchar (280) )
BEGIN
  if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = nomeUtente)) then
   start transaction;
    #Seleziono i nomi degli habitat e verifico che il Nome inserito non sia presente nei nomi estratti dalla query
    INSERT INTO HABITAT(nome,nomeRicercatore,descrizione)VALUES(Nome,nomeUtente,Descrizione);
   commit work;
  else
   call printf("[ERRORE] Non sei un ricercatore!");
  end if;
END;
|

|
CREATE PROCEDURE Iscrizione
(IN NomeProfilo varchar(30), IN IDEscursione int)
BEGIN
   /**
    * Controllo che i partecipanti ad una data escursione presa in input (IDEscursione) siano
    * minori del numero massimo di partecipanti.
    * Inoltre controllo la tipologia di colui che si vuole iscrivere
    */
   DECLARE NumeroPartecipanti int;
   DECLARE CursorePartecipanti CURSOR FOR (SELECT nPartecipanti
                              FROM ESCURSIONE
                              WHERE (id=IDEscursione));
   OPEN CursorePartecipanti;
   FETCH CursorePartecipanti INTO NumeroPartecipanti;
   #CALL printf("NumeroPartecipanti: "+NumeroPartecipanti);
   IF(   NumeroPartecipanti  > any (SELECT COUNT(*)
                              FROM PARTECIPAZIONE_ESCURSIONI
                              WHERE (idEscursione=IDEscursione ) )
  AND (
    "UTENTE SEMPLICE"= any (SELECT tipoAccount
                   FROM PROFILO
                   WHERE nome=NomeProfilo)
  OR
    "UTENTE PREMIUM"= any (SELECT tipoAccount
                   FROM PROFILO
                   WHERE nome=NomeProfilo) ) )
  THEN
    start transaction;
    INSERT INTO PARTECIPAZIONE_ESCURSIONI(idEscursione,nomeProfilo) VALUES(IDEscursione, NomeProfilo);
    CLOSE CursorePartecipanti;
    commit;
   ELSE
    CALL printf("[ERRORE]Non è possibilie effettuare l'iscrizione.");
   END IF;
END;
|

|
CREATE PROCEDURE creaProfilo
(IN nomeInserito varchar (30), IN tipoAccount enum ('UTENTE SEMPLICE', 'UTENTE PREMIUM', 'RICERCATORE'),
 IN email varchar (30), IN password varchar (32), IN foto longblob, IN professione varchar (30),
  IN annoNascita int)
BEGIN
  #Prima di creare il PROFILO controllo che non sia già presente
  IF(nomeInserito NOT IN (SELECT nome FROM PROFILO)) THEN
    start transaction;
    INSERT INTO PROFILO(nome, tipoAccount, email, password, foto, professione, annoNascita) VALUES (nomeInserito, tipoAccount , email, password, foto, professione, annoNascita);
    commit;
  ELSE
    CALL printf("[ERRORE]Non è possibilie creare il PROFILO. È già presente. ");
  END IF;
END;
|

|
create procedure NuovaDonazione (in nome varchar(30) , in importo double, in note varchar(280), in idRaccolta int)
begin
  # verifico che la raccolta sia aperta e che una volta versato l'importo non si ecceda l'importo massimo
  if (("aperto") = all (select stato from RACCOLTA_FONDI where id = idRaccolta) and
   (importo <= (select importoMax - importoRaccolto as rimanente from RACCOLTA_FONDI where id = idRaccolta))) then
    start transaction;
      insert into DONAZIONE(nomeDonatore, idRaccolta, note, importo) values (nome, idRaccolta, note, importo);
      update RACCOLTA_FONDI set importoRaccolto = importoRaccolto + importo where id = idRaccolta;
    commit work;
  else
   # restituire poi a video all utente un messaggio simile
   call printf("[ERRORE] L'importo inserito eccede quello massimo della raccolta ");
  end if;
end;
|

|
create procedure NuovaRaccoltaFondi (in nomeUtente varchar(30), in nome varchar(30), in importo double, in descrizione text, in dataInizio date)
begin
# prima di preocedere verifico che l utente che vuole creare la raccolta sia di tipo ricercatore
if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = nomeUtente)) then
start transaction;
insert into RACCOLTA_FONDI(nomeCreatore,nome, stato, importoMax, descrizione, dataInizio)
  values (nomeUtente,nome ,'aperto', importo, descrizione, dataInizio);
commit work;
end if;
end;
|

|
create procedure InvioMessaggio (in mittente varchar(30), in destinatario varchar(30), in testo varchar(280), in titolo varchar(30))
begin
if (destinatario = any (select nome from PROFILO)) then
start transaction;
insert into MESSAGGIO(mittente, destinatario, timeStamp, testo, titolo)
  values (mittente, destinatario, current_timestamp, testo, titolo);
commit work;
else
# restituire poi a video all utente un messaggio simile
call printf("[ERRORE] Il destinatario non esiste nella piattaforma");
end if;
end;
|

|
# non cè nome specie perchè viene inserito solo se ci sono almeno 5 proposte o
# se lo inserisce un ricercatore
create procedure NuovaSegnalazione (in nomeUtente varchar(30), in nomeHabitat varchar (50),
            in latitudine double, in longitudine double, in foto longblob)
begin
declare tipo varchar(30);
select tipoAccount into tipo from PROFILO where nome = nomeUtente;
if  (("UTENTE SEMPLICE" = tipo or "UTENTE PREMIUM" = tipo) and nomeHabitat = any (select nome from HABITAT)) then
start transaction;
insert into SEGNALAZIONE(nomeUtente, nomeHabitat, latitudine, longitudine, foto, data)
  values (nomeUtente, nomeHabitat, latitudine, longitudine, foto, current_date);
commit work;
else
# restituire poi a video all utente un messaggio simile
call printf("[ERRORE] Sei un ricercatore o l'habitat non è presente nella piattaforma!");
end if;
end;
|

|
create procedure NuovoAnimale (in nomeLatino varchar (30), in nomeRicercatore varchar (30), in livVulnerabilità varchar (30),
                     in annoClassificazione int, in nomeItaliano varchar (30), in classe varchar (30),
                     in linkWiki varchar (90), in peso double, in altezza double, in nProle int)
begin
if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = nomeRicercatore)) then
start transaction;
insert into SPECIE values (nomeLatino, nomeRicercatore, livVulnerabilita, annoClassificazione, nomeItaliano, classe, linkWiki);
insert into ANIMALE values (nomeLatino, peso, altezza, nProle);
commit work;
else
# restituire poi a video all utente un messaggio simile
call printf("[ERRORE] Non sei un ricercatore!");
end if;
end;
|

|
create procedure NuovoVegetale (in nomeLatino varchar (30), in nomeRicercatore varchar (30), in livVulnerabilità varchar (30),
                    in annoClassificazione int, in nomeItaliano varchar (30), in classe varchar (30),
                    in linkWiki varchar (90), in altezza double, in diametro double)
begin
if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = nomeRicercatore)) then
start transaction;
insert into SPECIE values (nomeLatino, nomeRicercatore, livVulnerabilita, annoClassificazione, nomeItaliano, classe, linkWiki);
insert into VEGETALE values (nomeLatino, altezza, diametro);
commit work;
else
# restituire poi a video all utente un messaggio simile
call printf("[ERRORE] Non sei un ricercatore!");
end if;
end;
|


|
create procedure NuovaProposta (in nomeUtente varchar(30), in codiceSegnalazione int, in specie varchar(30), in commento varchar(280))
begin
if (
   (codiceSegnalazione = any(select codice from SEGNALAZIONE))
   and
   (specie = any(select nomeLatino from SPECIE))
   and
   (   "UTENTE SEMPLICE"= any (SELECT tipoAccount
                      FROM PROFILO
                      WHERE nome=nomeUtente)
      or
      "UTENTE PREMIUM"= any ( SELECT tipoAccount
                      FROM PROFILO
                      WHERE nome=nomeUtente))) then
  start transaction;
if (nomeUtente = any (select nomeUtente from PROPOSTA where PROPOSTA.codiceSegnalazione = codiceSegnalazione)) then
   delete from PROPOSTA where PROPOSTA.nomeUtente = nomeUtente and PROPOSTA.codiceSegnalazione = codiceSegnalazione;
end if;
  start transaction;
   insert into PROPOSTA (nomeUtente, codiceSegnalazione, specie, data, commento) values (nomeUtente, codiceSegnalazione, specie, current_date, commento);
  commit work;
else
  # restituire poi a video all utente un messaggio simile
  call printf("[ERRORE] Errore nell'inserimento della PROPOSTA");
end if;
end;
|

|
CREATE PROCEDURE ModificaHabitat
(IN Nome varchar (50),IN nomeUtente varchar (30),IN Descrizione varchar (280) )
BEGIN
  if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = nomeUtente)
    and
    Nome = any (select nome from HABITAT)) then
   start transaction;
    update HABITAT set descrizione = Descrizione where HABITAT.nome = Nome;
   commit work;
  else
   call printf("[ERRORE] Non sei un ricercatore! oppure non eseiste l'habitat che vuoi aggiornare");
  end if;
END;
|

|
create procedure ModificaAnimale (in NomeLatino varchar (30), in NomeRicercatore varchar (30), in LivVulnerabilità varchar (30),
                     in AnnoClassificazione int, in NomeItaliano varchar (30), in classe varchar (30),
                     in LinkWiki varchar (90), in Peso double, in Altezza double, in NProle int)
begin
if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = nomeRicercatore)
  and
  NomeLatino = any (select nomeLatino from ANIMALE)) then
  start transaction;
  #if(NomeLatino=any(select nomeLatino from specie))then
   update SPECIE set nomeRicercatore = NomeRicercatore, livVulnerabilità = LivVulnerabilità, annoClassificazione = AnnoClassificazione, nomeItaliano = NomeItaliano, classe = Classe, linkWiki = LinkWiki
   where SPECIE.nomeLatino = NomeLatino;
   update ANIMALE set peso = Peso, altezza = Altezza, nProle = NProle
   where ANIMALE.nomeLatino = NomeLatino;
   commit work;
  #end if;

else
  # restituire poi a video all utente un messaggio simile
  call printf("[ERRORE] Non sei un ricercatore! oppure la specie che vuoi modificare non esiste");
end if;
end;
|

|
create procedure ModificaVegetale (in NomeLatino varchar (30), in NomeRicercatore varchar (30), in LivVulnerabilità varchar (30),
                    in AnnoClassificazione int, in NomeItaliano varchar (30), in Classe varchar (30),
                    in LinkWiki varchar (90), in Altezza double, in Diametro double)
begin
if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = nomeRicercatore)
  and
  NomeLatino = any (select nomeLatino from specie)) then
  start transaction;
  update SPECIE set nomeRicercatore = NomeRicercatore, livVulnerabilità = LivVulnerabilità, annoClassificazione = AnnoClassificazione, nomeItaliano = NomeItaliano, classe = Classe, linkWiki = LinkWiki
   where SPECIE.nomeLatino = NomeLatino;
  update VEGETALE set altezza = Altezza, diametro = Diametro
   where VEGETALE.nomeLatino = NomeLatino;
  commit work;
else
  # restituire poi a video all utente un messaggio simile
  call printf("[ERRORE] Non sei un ricercatore! oppure la specie che vuoi modificare non esiste");
end if;
end;
|


|
# procedura che permette al ricercatore di intervenire e aggiustare/aggiungere una classificazione
create procedure CorreggiClassificazione (in mioNomeUtente varchar(30), in codSegnalazione int,  in specie varchar(30))
begin
# prima di preocedere verifico che l utente che vuole creare la raccolta sia di tipo ricercatore
if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = mioNomeUtente)) then
  start transaction;
  update SEGNALAZIONE set nomeSpecie = specie where codice = codSegnalazione;
  commit work;
end if;
end;
|

|
create procedure EliminaHabitat (in mioNomeUtente varchar(30), in nomeHabitat varchar(50))
begin
# prima di preocedere verifico che l utente che vuole creare la raccolta sia di tipo ricercatore
if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = mioNomeUtente)) then
  start transaction;
   delete from HABITAT where nome = nomeHabitat;
  commit work;
end if;
end;
|

|
create procedure EliminaSpecie (in mioNomeUtente varchar(30), in nomeSpecie varchar(50))
begin
# prima di preocedere verifico che l utente che vuole creare la raccolta sia di tipo ricercatore
if ("RICERCATORE" = any (select tipoAccount from PROFILO where PROFILO.nome = mioNomeUtente)) then
  start transaction;
   delete from SPECIE where nomeLatino = nomeSpecie;
   delete from ANIMALE where nomeLatino = nomeSpecie;
   delete from VEGETALE where nomeLatino = nomeSpecie;
  commit work;
end if;
end;
|

|
CREATE PROCEDURE aggiornaProfilo
(IN nomeInserito varchar (30),
 IN nuovaEmail varchar (30), IN nuovaPassword varchar (32), IN nuovaFoto longblob, IN nuovaProfessione varchar (30),
  IN nuovoAnnoNascita int)
BEGIN
  #Prima di creare il PROFILO controllo che non sia già presente
  IF(nomeInserito = any(SELECT nome FROM PROFILO)) THEN
    start transaction;
    update PROFILO set email = nuovaEmail, password = nuovaPassword, foto = nuovaFoto, professione = nuovaProfessione, annoNascita = nuovoAnnoNascita
     where nome = nomeInserito;
    commit;
  ELSE
    CALL printf("[ERRORE]Non è possibilie creare il PROFILO. È già presente. ");
  END IF;
END;
|

delimiter ;

# autocommit viene riportato a 1
SET AUTOCOMMIT = 1;

# ------  VIEWS ----------

create view UtentiPremium(nome, indice) as
  select nome, nCorrette/(nCorrette+nErrate) as indice
  from PROFILO
  where tipoAccount = "UTENTE PREMIUM"
  order by nCorrette/(nCorrette+nErrate) desc;

create view ClassificaSpecie (nomeSpecie, quantita) as
  select nomeSpecie, count(*) as quantita
  from SEGNALAZIONE
  group by nomeSpecie
  order by count(nomeSpecie) desc;

create view UtentiAttivi (nomeUtente, nSegnalazioni) as
  select nome, nSegnalazioni
  from PROFILO
  where tipoAccount<>'RICERCATORE'
  order by nSegnalazioni desc;

create view CampagneFondi (nomeCreatore, nome, importoRaccolto, importoMax, descrizione, dataInizio) as
  select nomeCreatore, nome, importoRaccolto, importoMax, descrizione, dataInizio
  from RACCOLTA_FONDI
  where stato = "aperto";

create view EscursioniPresenti(id, titolo,nPartecipanti) AS
select id, titolo, nPartecipanti
from ESCURSIONE;



# Popolamento del database a fini di testing
LOAD DATA LOCAL INFILE '../popolamento/Habitat.csv' INTO TABLE HABITAT FIELDS TERMINATED BY ';'
ENCLOSED BY '"'LINES TERMINATED BY '\n'IGNORE 1 ROWS;

LOAD DATA LOCAL INFILE '../popolamento/Specie.csv' INTO TABLE SPECIE FIELDS TERMINATED BY ';'
ENCLOSED BY '"'LINES TERMINATED BY '\n'IGNORE 1 ROWS;

LOAD DATA LOCAL INFILE '../popolamento/Segnalazione.csv' INTO TABLE SEGNALAZIONE FIELDS TERMINATED BY ';'
ENCLOSED BY '"'LINES TERMINATED BY '\n'IGNORE 1 ROWS;

LOAD DATA LOCAL INFILE '../popolamento/Profilo.csv' INTO TABLE PROFILO FIELDS TERMINATED BY ';'
ENCLOSED BY '"'LINES TERMINATED BY '\n'IGNORE 1 ROWS;

LOAD DATA LOCAL INFILE '../popolamento/Animali.csv' INTO TABLE ANIMALE FIELDS TERMINATED BY ';'
ENCLOSED BY '"'LINES TERMINATED BY '\n'IGNORE 1 ROWS;

LOAD DATA LOCAL INFILE '../popolamento/Vegetali.csv' INTO TABLE VEGETALE FIELDS TERMINATED BY ';'
ENCLOSED BY '"'LINES TERMINATED BY '\n'IGNORE 1 ROWS;

call NuovaSegnalazione("ric", "Bassa montagna", 28.7129273, 34.7381937, "ecco la foto");
call NuovaProposta("ric", 56, "Vulpes_Vulpes", "le orecchie seono caratteristiche della volpe");
call NuovaProposta("sgarbi", 56, "Vulpes_Vulpes", "la coda è caratteristica della volpe");
call NuovaProposta("zlatan", 56, "Ovis_musimon", "mi sembrano le corna di un muflone");
call NuovaProposta("xxxxxxxxxx", 56, "Ovis_musimon", "il colore della pelliccia è quello del muflone");
call NuovaProposta("uuuuuuuuuu", 56, "Vulpes_Vulpes", "è una volpe senza dubbio");

