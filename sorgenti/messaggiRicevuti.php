<?php
  include 'connection.php';
  session_start();
  if(!isset($_SESSION['userSemplice']) and !isset($_SESSION['userPremium']) and !isset($_SESSION['ricercatore'])){
    //echo (.$user);
    header("location:home.html");
  }
?>
<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="Web2.css">
   <title>Messaggi</title>
</head>
<body>
  <div class="p-3 mb-2 bg-primary text-white"><p align="left"><strong><h1>I tuoi messaggi</h1></strong></p></div>
  <div class="container" align="center">
    <?php
      if(isset($_SESSION['userSemplice'])){
        echo "<h3> Ciao ".$_SESSION['userSemplice']."!";
      }
      else if(isset($_SESSION['userPremium'])){
        echo "<h3> Ciao ".$_SESSION['userPremium']."!";
      }
      else{
        echo "<h3> Ciao ".$_SESSION['ricercatore']."!";
      }
    ?>
    <br>
    <div class="p-3 mb-2 bg-info text-white">Messaggi ricevuti</div>
    <?php
    try{
      $sql='SELECT * FROM MESSAGGIO';
      $res=$pdo->query($sql);
      echo'<table class= "table table-hover table-dark">';
      echo"<tr>";
      echo'<th scope="col">';
      echo"Mittente";
      echo"</th>";
      echo'<th scope="col">';
      echo"Titolo del messaggio";
      echo"</th>";
      echo'<th scope="col">';
      echo"Testo del messaggio";
      echo"</th>";
      echo"</tr>";    
      while($row=$res->fetch()) {
        if(isset($_SESSION['userSemplice'])){
          if($row['destinatario']==$_SESSION['userSemplice']){
            echo'<tr>';
            echo"<td>".$row['mittente']."</td>";
            echo"<td>".$row['titolo']."</td>";
            echo"<td>".$row['testo']."</td>";
            echo"</tr>";
          }
        }
        else if(isset($_SESSION['userPremium'])){
          if($row['destinatario']==$_SESSION['userPremium']){
            echo'<tr>';
            echo"<td>".$row['mittente']."</td>";
            echo"<td>".$row['titolo']."</td>";
            echo"<td>".$row['testo']."</td>";
            echo"</tr>";
          }
        }
        else{
          if($row['destinatario']==$_SESSION['ricercatore']){
            echo'<tr>';
            echo"<td>".$row['mittente']."</td>";
            echo"<td>".$row['titolo']."</td>";
            echo"<td>".$row['testo']."</td>";
            echo"</tr>";
          }
        }
      }
      echo"</table>";
      $sql='SELECT * FROM MESSAGGIO';
      $res2=$pdo->query($sql);
      echo'<div class="p-3 mb-2 bg-success text-white">Messaggi inviati</div>';
      echo'<table class= "table table-striped">';
      echo"<tr>";
      echo'<th scope="col">';
      echo"Mittente";
      echo"</th>";
      echo'<th scope="col">';
      echo"Destinatario";
      echo"</th>";
      echo'<th scope="col">';
      echo"Titolo del messaggio";
      echo"</th>";
      echo'<th scope="col">';
      echo"Testo del messaggio";
      echo"</th>";
      echo"</tr>"; 
      while($row2=$res2->fetch()) {
        if(isset($_SESSION['userSemplice'])){
          if($row2['mittente']==$_SESSION['userSemplice']){
            echo'<tr>';
            echo"<td>".$row2['mittente']."</td>";
            echo"<td>".$row2['destinatario']."</td>";
            echo"<td>".$row2['titolo']."</td>";
            echo"<td>".$row2['testo']."</td>";
            echo"</tr>";
          }
        }
        else if(isset($_SESSION['userPremium'])){
          if($row2['mittente']==$_SESSION['userPremium']){
            echo'<tr>';
            echo"<td>".$row2['mittente']."</td>";
            echo"<td>".$row2['destinatario']."</td>";
            echo"<td>".$row2['titolo']."</td>";
            echo"<td>".$row2['testo']."</td>";
            echo"</tr>";
          }
        }
        else{
          if($row2['mittente']==$_SESSION['ricercatore']){
            echo'<tr>';
            echo"<td>".$row2['mittente']."</td>";
            echo"<td>".$row2['destinatario']."</td>";
            echo"<td>".$row2['titolo']."</td>";
            echo"<td>".$row2['testo']."</td>";
            echo"</tr>";
          }
        }
      }
      echo'</table>';
      
    ?>
  </div>
  <?php
  if(isset($_SESSION['userSemplice'])){
        echo '<p align="left"><a href="profiloSemplice.php">Torna alla tua area personale</a></p>';
      }
      else if(isset($_SESSION['userPremium'])){
        echo '<p align="left"><a href="profiloPremium.php">Torna alla tua area personale</a></p>';
      }
      else{
        echo '<p align="left"><a href="profiloRicercatore.php">Torna alla tua area personale</a></p>';
      }
      echo '<p align="left"><a href="FormMessaggi.php">Invia un messaggio</a></p>';
    }
    catch(PDOException $e) {
       echo("Errore esecuzione query.");
       exit();
    }
  ?>
</body>
</html>
