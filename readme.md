# Database project year 2019/20

This is the main project of the databases course at UniBo inspired by the platform iNaturalist.
The aim is to create a database to store and manage sightings reports of different species around the world. With the help of researchers users this reports are then classified.
The platform is accessible through a simple website.

## Authors

* **Riccardo Cavallin** <riccardo.cavallin@studio.unibo.it>
* **Emanuele Fazzini** <emanuele.fazzini@studio.unibo.it>
* **Martino Simonetti** <martino.simonetti@studio.unibo.it>
* **Michele Bonini** <michele.bonini2@studio.unibo.it>


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

